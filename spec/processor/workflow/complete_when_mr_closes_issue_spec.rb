# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/workflow/complete_when_mr_closes_issue'

RSpec.describe Triage::Workflow::CompleteWhenMrClosesIssue do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'merge',
        from_gitlab_org?: from_gitlab_org,
        label_names: label_names,
        event_actor_username: 'user',
        iid: merge_request_id,
        project_id: project_id
      }
    end
  end

  let(:from_gitlab_org) { true }
  let(:label_names)     { ['group::gitaly'] }
  let(:project_id) { 1123 }
  let(:merge_request_id) { 11 }
  let(:issue_id) { 12 }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge']

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when no labels are set' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when "group::gitaly" is set' do
      let(:from_gitlab_org) { true }
      let(:label_names) { ["group::gitaly"] }

      include_examples 'event is applicable'
    end

    context 'when "group::not_gitaly" is set' do
      let(:from_gitlab_org) { true }
      let(:label_names) { ["group::not_gitaly"] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    it 'adds a label to issues' do
      issue = double("issue")
      expect(issue).to receive(:iid).and_return(issue_id)

      expect(Triage.api_client).to receive(:merge_request_closes_issues)
        .with(project_id, merge_request_id).and_return([issue])
      expect(subject).to receive(:add_comment)
        .with('/label ~"workflow::complete"',
          "/projects/#{project_id}/issues/#{issue_id}",
          append_source_link: false)

      subject.process
    end
  end
end
