# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class SecurityBranch < Base
        SLACK_CHANNEL = 'f_upcoming_release'
        SLACK_USERNAME = 'GitLab Release Tools Bot'
        SLACK_ICON = 'ci_failing'
        SLACK_PAYLOAD_TEMPLATE = <<~JSON
          {
            "text": "<!subteam^S0127FU8PDE> ☠️  Pipeline for merged result failed! ☠️ See %<pipeline_link>s (triggered from %<merge_request_link>s)"
          }
        JSON

        def self.match?(event)
          event.project_path_with_namespace == 'gitlab-org/security/gitlab' &&
            event.source == 'merge_request_event' &&
            event.merge_request.target_branch == 'master' &&
            event.event_actor == 'gitlab-release-tools-bot' &&
            event.source_job_id.nil?
        end

        def slack_channel
          SLACK_CHANNEL
        end

        def slack_username
          SLACK_USERNAME
        end

        def slack_icon
          SLACK_ICON
        end
      end
    end
  end
end
