# frozen_string_literal: true

require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class FailureTrace
      FAILURE_TRACE_MARKERS = {
        rspec: { start: "Failures:\n", end: "\n[TEST PROF INFO]", language: 'ruby' },
        jest: { start: "Summary of all failing tests\n", end: "\nRan all test suites.", language: 'javascript' }
      }.freeze

      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human

      attr_reader :trace, :config, :framework

      def initialize(trace:, config:)
        @trace = trace
        @config = config
        @framework = identify_framework
      end

      def summary_markdown
        return unless framework

        <<~MARKDOWN.chomp
        ```#{markers[:language]}
        #{failure_summary[...FAILURE_TRACE_SIZE_LIMIT]}
        ```
        MARKDOWN
      end

      def root_cause_label
        label_key = config.root_cause_to_trace_map.detect do |root_cause, trace_patterns|
          trace_patterns.any? { |pattern| trace.downcase.include?(pattern) }
        end&.first || :default

        Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.fetch(label_key)
      end

      private

      def identify_framework
        FAILURE_TRACE_MARKERS.each do |framework, markers|
          return framework if trace.include?(markers[:start]) && trace.include?(markers[:end])
        end

        nil
      end

      def failure_summary
        trace.split(markers[:start]).last.split(markers[:end]).first.chomp
      end

      def markers
        FAILURE_TRACE_MARKERS.fetch(framework)
      end
    end
  end
end
