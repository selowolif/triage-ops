# frozen_string_literal: true
require_relative '../triage'
require_relative '../triage/event'
require_relative '../../lib/devops_labels'
require_relative '../../lib/constants/labels'
require_relative '../resources/issue'

module Triage
  class DevopsLabelsValidator
    include DevopsLabels::Context

    attr_reader :event

    def initialize(event)
      @event = event
      @label_names = labels_to_check
    end

    def labels_set?
      special_team_labels? || has_group_label? || has_category_label?
    end

    private

    def special_team_labels?
      (event.label_names & Labels::SPECIAL_TEAM_LABELS).flatten.any?
    end

    def labels_to_check
      issue_labels | event.label_names
    end

    def issue_labels
      issue.labels
    end

    def issue
      @issue ||= Triage::Issue.new(Triage.api_client.issue(event.project_id, event.iid).to_h)
    end
  end
end
