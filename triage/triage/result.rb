# frozen_string_literal: true

module Triage
  class Result
    def errors
      @errors ||= []
    end

    def warnings
      @warnings ||= []
    end
  end
end
