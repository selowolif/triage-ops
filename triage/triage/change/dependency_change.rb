# frozen_string_literal: true

require_relative '../change'

module Triage
  class DependencyChange < Change
    def self.file_patterns
      # See https://gitlab.com/gitlab-org/gitlab/-/blob/7565bc6a7d36d00ead0b4d3e2fb6299284fcb063/.gitlab/ci/rules.gitlab-ci.yml#L252
      # Look for .dependency-patterns
      # To make styles more consistent, disable this cop
      # rubocop:disable Style/RegexpLiteral
      @file_patterns ||=
        %r{ \bGemfile\.lock$
          | \bcomposer\.lock$
          | \bgems\.locked$
          | \bgo\.sum$
          | \bnpm-shrinkwrap\.json$
          | \bpackage-lock\.json$
          | \byarn\.lock$
          | \bpackages\.lock\.json$
          | \bconan\.lock$
          }x
      # rubocop:enable Style/RegexpLiteral
    end
  end
end
