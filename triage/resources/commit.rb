# frozen_string_literal: true

module Triage
  class Commit
    attr_reader :attributes

    def initialize(attributes)
      @attributes = attributes
    end

    def header
      @header ||= attributes['message'].lines.first.strip
    end
  end
end
